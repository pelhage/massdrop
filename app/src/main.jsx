var React = require('react');
var ReactDOM = require('react-dom');
var JobQueue = require('./job-queue.jsx');
var AddJob = require('./add-job.jsx');

/* API endpoints go here */
var options = {
  queue: '/v1/queue'
};

var App = React.createClass({
  render: function() {
    return (
      <div className="mdl-card mdl-shadow--2dp">
        <h1>WebScrapr</h1>
        <JobQueue source={this.props.queue} />
      </div>
    );
  }
});

// 
var element = React.createElement(App, options);
ReactDOM.render(element, document.querySelector('.container'));