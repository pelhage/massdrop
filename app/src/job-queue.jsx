var React = require('react');

module.exports = React.createClass({
  
  getInitialState() {
    return {
      data: [],  
    };
  },

  componentDidMount() {
    this._loadQueue();
  },

  _loadQueue: function() {
    return fetch(this.props.source)
    .then(function(res) {
      return res.json();
    }).then(function(text){
      this.setState({data: text})
    }.bind(this)).catch(function(err) {
      console.log(err);
    })
  },
  
  render: function() {
    var ids = this.state.data.map(function(obj, index) {
      return (
        <li className="mdl-list__item" key={obj._id}>
          <div>
            <span className="mdl-list__item-primary-content">
              {obj.url}
            </span>
          </div>
          <span>{obj._id}</span>
        </li>
      );
    });
    return (
      <ul className="mdl-list">
        {ids}
      </ul>);
  }
});