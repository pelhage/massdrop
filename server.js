var app = require('express')();
var express = require('express');
var bodyParser = require('body-parser');
var mongodb = require('mongodb');
var ObjectID = require('mongodb').ObjectID;
var request = require('request');
var cheerio = require('cheerio');


// for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false })); 

// Global database variables
var MONGODB_URI = 'mongodb://localhost:27017/massdrop';
var db;
var queue;


// Initialize connection
mongodb.MongoClient.connect(MONGODB_URI, function (err, database) {
  if (err) { throw err; }
  
  db = database;
  queue = db.collection('queue');
  
  app.listen(3000);
  console.log('Listening on port 3000');
});


app.use('/', express.static(__dirname + '/app'));


app.use('/v1/jobs/scrape/:id', function(req, res) {
  var jobId = req.params.id;
  
  queue.findOne({'_id': new ObjectID(jobId)}, {url: 1, _id: 0}, function(err, document) {
    var url = document.url;
    request({uri: url}, function(error, response, body) { 
      queue.update({'_id': new ObjectID(jobId)}, { $set: {html: body}});
      res.write(body);
      res.end();
    });
  });
});

// See the entire queue of url's
app.get('/v1/queue', function(req, res) {
  queue.find().toArray(function (err, result) {
    if (err) { console.log(err); } 

    else if (result.length) {
      res.write(JSON.stringify(result));
      res.end();
    } else {
      res.end();
      console.log('No document(s) found with defined "find" criteria!');
    }
  });
});


// Get individual job
app.get('/v1/jobs/:id', function(req, res) {
  var jobId = req.params.id;

  queue.find({"_id": new ObjectID(jobId)}).toArray(function(err, results) {
    res.write(JSON.stringify(results));
    res.end();
  });
});

// Add a url to be scraped to the queue
app.post('/v1/jobs', function(req, res) {
  var jobUrl = req.body.jobUrl;
  if (jobUrl)
  queue.insert({url: jobUrl, status: 'incomplete', html: null}, function(err, result) {
    console.log(result);
    res.send('Great success!');
    res.end();
  })
});