// `use massdrop`
var db = db.getSiblingDB('massdrop');

// delete any data already existing in DB
db.dropDatabase();


// create fake queue to use
var queueRaw = [
    { url: 'http://www.google.com' },
    { url: 'http://www.massdrop.com'},
    { url: 'http://www.yahoo.com'},
    { url: 'http://'}
    // Figure out structure of queue later
];

// insert queue in `queue` collection
db.queue.insert(queueRaw);