This is the repo for the Massdrop code screening/challenge/evaluation:

Create a job queue whose workers fetch data from a URL and store the results in a database.  The job queue should expose a REST API for adding jobs and checking their status / results.
 
Example:
 
User submits http://www.google.com <http://www.google.com/> to your endpoint. The user gets back a job id. Your system fetches http://www.google.com <http://www.google.com/> (the result of which would be HTML) and stores the result. The user asks for the status of the job id and if the job is complete, he gets a response that includes the HTML for http://www.google.com <http://www.google.com/>.
 
Use any tech you want, put code in a gist or on github and send it to mailto:darren.craine@massdrop.com

It will be coded using Node.js, MongoDB, and, perhaps RabbitMQ for middleware for the queue.


REST API:

GET: /v1/status/{id}
POST:  /v1/jobs/{id}
GET: /v1/jobs/{id}
